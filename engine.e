note
	description: "Th mecanical engine of the rainbow experiment."
	author: "Louis Marchand"
	date: "Mon, 23 Mar 2020 20:08:33 +0000"
	revision: "0.1"

class
	ENGINE

inherit
	GAME_LIBRARY_SHARED

create
	make

feature {NONE} -- Constants

	Width:INTEGER = 480
			-- The horizontal dimension of the scene

	Height:INTEGER = 250
			-- The horizontal dimension of the scene

feature {NONE} -- Initialisation

	make
			-- Initialisation of `Current'
		local
			l_builder:GAME_WINDOW_RENDERED_BUILDER
		do
			create l_builder
			l_builder.enable_must_renderer_synchronize_update
			l_builder.enable_fake_fullscreen
			window := l_builder.generate_window
			has_error := window.has_error
			create {ARRAYED_LIST[LIST[GAME_TEXTURE]]} textures.make (5)
			create {ARRAYED_LIST[INTEGER]}indexes.make (5)
			across 1 |..| 7 as la_index loop indexes.extend (1)  end
			create {ARRAYED_LIST[INTEGER]}maximum_indexes.make (5)
			is_paused := False
			if not has_error then
				window.renderer.set_logical_size (Width, Height)
				initialise_textures1
				initialise_textures2
				initialise_textures3
				initialise_textures4
				initialise_textures5
			end
		end

	initialise_texture(a_red, a_green, a_blue:REAL; a_radius:INTEGER):GAME_TEXTURE
			-- Initialisation of a color arc of color `a_red',`a_green' and `a_blue' of radius `a_radius'
		local
			l_cairo_surface:CAIRO_SURFACE_IMAGE
			l_context:CAIRO_CONTEXT
			l_pixel_format:CAIRO_PIXEL_FORMAT
			l_game_surface:GAME_SURFACE_CAIRO
		do
			create l_pixel_format.make_argb32
			create l_cairo_surface.make (l_pixel_format, Width, Height)
			create l_context.make (l_cairo_surface)
			l_context.set_source_rgb (a_red, a_green, a_blue)
			l_context.set_line_width (20)
			l_context.arc (Width // 2, Height, a_radius, {DOUBLE_MATH}.pi, 2 * {DOUBLE_MATH}.pi)
			l_context.stroke
			create l_game_surface.make_from_cairo_surface (l_cairo_surface)
			create Result.make_from_surface (window.renderer, l_game_surface)
		end

	initialise_textures1
			-- Initialise every textures of the first outer arc of the `textures'
		local
			l_textures:ARRAYED_LIST[GAME_TEXTURE]
		do
			create l_textures.make (1)
			l_textures.extend (initialise_texture(1.0, 0.0, 0.0, 220))	-- Red
			textures.extend (l_textures)
			maximum_indexes.extend (1)
		end

	initialise_textures2
			-- Initialise every textures of the second outer arc of the `textures'
		local
			l_textures:ARRAYED_LIST[GAME_TEXTURE]
			l_red_texture:GAME_TEXTURE
		do
			create l_textures.make (3)
			l_red_texture := initialise_texture(1.0, 0.0, 0.0, 200)	-- Red
			l_textures.extend (l_red_texture)
			l_textures.extend (initialise_texture(0.0, 1.0, 0.0, 200))	-- Green
			l_textures.extend (l_red_texture)	-- Red
			textures.extend (l_textures)
			maximum_indexes.extend (3)
		end

	initialise_textures3
			-- Initialise every textures of the third outer arc of the `textures'
		local
			l_textures:ARRAYED_LIST[GAME_TEXTURE]
		do
			create l_textures.make (2)
			l_textures.extend (initialise_texture(1.0, 0.0, 0.0, 180))	-- Red
			l_textures.extend (initialise_texture(0.0, 1.0, 0.0, 180))	-- Green
			textures.extend (l_textures)
			maximum_indexes.extend (2)
		end

	initialise_textures4
			-- Initialise every textures of the fourth outer arc (second inner arc) of the `textures'
		local
			l_textures:ARRAYED_LIST[GAME_TEXTURE]
		do
			create l_textures.make (2)
			l_textures.extend (initialise_texture(0.0, 1.0, 0.0, 160))	-- Green
			l_textures.extend (initialise_texture(0.0, 0.0, 1.0, 160))	-- Blue
			textures.extend (l_textures)
			maximum_indexes.extend (2)
		end

	initialise_textures5
			-- Initialise every textures of the fifth outer arc (first inner arc) of the `textures'
		local
			l_textures:ARRAYED_LIST[GAME_TEXTURE]
		do
			create l_textures.make (2)
			l_textures.extend (initialise_texture(1.0, 0.0, 0.0, 140))	-- Red
			l_textures.extend (initialise_texture(0.0, 0.0, 1.0, 140))	-- Blue
			textures.extend (l_textures)
			maximum_indexes.extend (2)
		end

feature -- Access

	has_error:BOOLEAN
			-- An error occured

	window:GAME_WINDOW_RENDERED
			-- Where to draw the scene

	textures:LIST[LIST[GAME_TEXTURE]]
			-- Every image to show on the `window'

	indexes:LIST[INTEGER]
			-- Current index of every list of `textures'

	maximum_indexes:LIST[INTEGER]
			-- Maximum `indexes' of every list of `textures'

	is_paused:BOOLEAN
			-- The animation must stop updating

	run
			-- Execute `Current'
		require
			No_Error: not has_error
		do
			game_library.iteration_actions.extend (agent on_iteration)
			game_library.quit_signal_actions.extend (agent on_quit)
			window.key_pressed_actions.extend (agent on_key_pressed)
			if window.renderer.driver.is_present_synchronized_supported then
				game_library.launch_no_delay
			else
				game_library.launch
			end

		end

feature {NONE} -- Implementation

	on_iteration(a_timestamp:NATURAL)
			-- At each game loop
		local
			l_renderer:GAME_RENDERER
			l_cursor:INDEXABLE_ITERATION_CURSOR[LIST[GAME_TEXTURE]]
		do
			l_renderer := window.renderer
			if not is_paused then
				l_renderer.clear
				from
					l_cursor := textures.new_cursor
				until
					l_cursor.after
				loop
					l_renderer.draw_texture (l_cursor.item.at (indexes.at (l_cursor.cursor_index)), 0, 0)
					indexes.at (l_cursor.cursor_index) := (indexes.at (l_cursor.cursor_index) \\ maximum_indexes.at (l_cursor.cursor_index)) + 1
					l_cursor.forth
				end
			end
			l_renderer.present
		end

	on_key_pressed(a_timestamp:NATURAL_32; a_key_state:GAME_KEY_EVENT)
			-- When the user pressed a key
		do
			if not a_key_state.is_repeat then
				if a_key_state.is_space then
					is_paused := not is_paused
				elseif a_key_state.is_escape then
					game_library.stop
				end
			end
		end

	on_quit(a_timestamp:NATURAL)
			-- When the application has to close `Current'
		do
			game_library.stop
		end

end
