Rainbow experiment
==================

This is a little experimentation that present a rainbow at the user presenting 5 colors: red, orange, yellow, pale blue and magenta. The fact is that, at no momment did the program show a color other than pure red, blue and green color. The rainbow colors are entirely interpreted by the brain. It can be usefull to explain how the human see colors.

Compilation
-----------
To compile it, you need the Eiffel_Cairo (https://github.com/tioui/Eiffel_Cairo) and Eiffel_Game2 (https://github.com/tioui/Eiffel_Game2) library.

Once those are installed, open the rainbow.ecf file in EiffelStudio and compile the project.

Do not forget to add the binary library (via package manager on linux and DLLs on Windows)

License
-------
MIT License

Copyright (c) 2020 Louis Marchand

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
