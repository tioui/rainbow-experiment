note
	description : "A rainbow experiment."
	author      : "Louis Marchand"
	generator   : "Eiffel Game2 Project Wizard"
	date        : "2020-03-22 02:33:08.759 +0000"
	revision    : "0.1"

class
	APPLICATION

inherit
	GAME_LIBRARY_SHARED

create
	make

feature {NONE} -- Initialization

	make
			-- Running the game.
		local
			l_engine:ENGINE
		do
			game_library.enable_video
			create l_engine.make
			if not l_engine.has_error then
				l_engine.run
			end
		end

end
